package com.verizon.learning.controllers;

import com.verizon.learning.valueobjects.Customer;
import com.verizon.learning.valueobjects.CustomerAddress;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class InvoiceRestController {

    WebClient webClient = WebClient.create("http://localhost:8080");


    private Mono<Customer> getCustomerById(String customerId) {

        Mono<Customer> customer = webClient
                .get()
                .uri("/customers/{customerId}", customerId)
                .retrieve()
                .bodyToMono(Customer.class);

        return customer;

    }


    private Flux<CustomerAddress> getPostalAddresses(String customerId) {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("type", "P");

        Flux<CustomerAddress> customer = webClient
                .get()
                .uri(uriBuilder -> {

                    return
                            uriBuilder.path("/customers/{customerId}/addresses")
                                    .queryParam("type", "P")
                                    .build(customerId);

                })
                .retrieve()
                .bodyToFlux(CustomerAddress.class);

        WebSocketClient webSocketClient = new ReactorNettyWebSocketClient();



        return customer;

    }

    private Flux<CustomerAddress> getDeliveryAddresses(String customerId) {

        Flux<CustomerAddress> customer = webClient
                .get()
                .uri(uriBuilder -> {

                    return
                            uriBuilder.path("/customers/{customerId}/addresses")
                                    .queryParam("type", "D")
                                    .build(customerId);

                })
                .retrieve()
                .bodyToFlux(CustomerAddress.class);

        return customer;

    }

    @GetMapping(path = "/invoice-customers")
    public Flux<Customer> handleGetInvoiceCustomers() {


        // @formatter:off

        Flux<Customer> customers =
                webClient
                        .get()
                        .uri("/customers")
                        .retrieve()
                        .bodyToFlux(Customer.class);

        // @formatter:on

//		RestTemplate template = new RestTemplate();

//	    ResponseEntity<List<Customer>> customerList =	
//	    		template.exchange("http://asdad", HttpMethod.GET, null,
//	    				new ParameterizedTypeReference<List<Customer>>() {
//	    			}) ;
//
//	    System.out.println(customerList.getBody());
//	    

        return customers;
    }


    @GetMapping(path = "/invoice-customers/{customerId}")
    public Mono<Customer> handleGetCustomerById(@PathVariable String customerId) {

        Mono<Customer> customer = webClient
                .get()
                .uri("/customers/{customerId}", customerId)
                .retrieve()
                .bodyToMono(Customer.class);


        return customer;

    }


    @GetMapping(path = "/invoice-specific-customers")
    public Flux<Customer> handleGetCustomersBySpecificIds() {

        List<String> ids = Arrays.asList("101", "103", "105");


        Flux<Customer> customers =

                Flux.fromIterable(ids)
                        // transform String (id) > Customer
                        .flatMap(this::getCustomerById);

        return customers;


    }

    @GetMapping(path = "/inv-customer-addresses")
    public Flux<CustomerAddress> handleGetAddressesByCustomerId() {

        List<String> ids = Arrays.asList("102", "103", "105");

        // customers/{customerId}/addresses params = "type"
        // http://localhost:8080/customers/101/addresses?type=101

//        Flux<Flux<CustomerAddress>> customerAddresses =
//                Flux.fromIterable(ids)
//                        .map(x -> {
//
//                            String customerId = x;
//                            Flux<CustomerAddress> postalAddreses = this.getPostalAddresses(customerId);
//                            return postalAddreses;
//                        });

        Flux<CustomerAddress> postalAddresses =
                Flux.fromIterable(ids)
                        .flatMap(this::getPostalAddresses);


        Flux<CustomerAddress> addresses = null;

        return addresses;
    }

    /**
     * Your assignment
     *
     * @return
     */
    @GetMapping(path = "/customer-addresses-map0")
    public Flux<Customer> handleGetCustomerAddressMap0() {

        List<String> ids = Arrays.asList("101", "103", "105");

        Flux<Customer> customers =

                Flux.fromIterable(ids)
                        .flatMap(customerId -> {

//                Mono<Customer> monoCustomer =
                            return
                                    this.getCustomerById(customerId)
                                            .zipWhen(customer -> {

                                                Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(customer.getId());

                                                Flux<CustomerAddress> deliveryAddresses = this.getDeliveryAddresses(customer.getId());

                                                return Flux
                                                        .merge(postalAddresses, deliveryAddresses)
                                                        .collectList();

//                                                return postalAddresses.collectList();

                                            })
                                            .map(tuple2 -> {

                                                Customer c = tuple2.getT1();
                                                List<CustomerAddress> a = tuple2.getT2();
                                                c.setAddresses(a);

                                                return c;
                                            });
                        });


        return customers;

    }


    @GetMapping(path = "/grouped-addresses")
    public Flux<Map<String, List<CustomerAddress>>> handleGetGroupedAddresses() {

        List<String> ids = Arrays.asList("101", "102", "103", "104", "105");
        Flux<CustomerAddress> addresses = Flux.fromIterable(ids)
                .flatMap(customerId -> {

                    Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(customerId);

                    Flux<CustomerAddress> deliveryAddresses = this.getDeliveryAddresses(customerId);

                    return Flux
                            .merge(postalAddresses, deliveryAddresses);


                });

        Flux<GroupedFlux<String, CustomerAddress>> groupedAdresses =
                addresses
                        .groupBy(customerAddress -> customerAddress.getCustomerId());

        Flux<Map<String, List<CustomerAddress>>> mapOfAddresses =
                groupedAdresses
                        .flatMap(groupedFlux -> {
                            return
                                    groupedFlux
                                            .collectList()
                                            .map(addressesOfTheCustomer -> {

                                                Map<String, List<CustomerAddress>> map = new HashMap<>();
                                                map.put(groupedFlux.key(), addressesOfTheCustomer);
                                                return map;

                                            });
                        });

        return mapOfAddresses;

    }


    @GetMapping(path = "/customer-addresses-map")
    public Mono<Customer> handleGetCustomerAddressMap() {

        List<String> ids = Arrays.asList("101", "103", "105");
        /**
         * Give me the Customer objects with its respective postalAddresses.
         */

        String customerId = "101";

//        Mono<Tuple2<Customer, List<CustomerAddress>>> monoCustomer =
//                this.getCustomerById(customerId)
//                        .zipWhen(customer -> {
//
//                            Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(customer.getId());
//                            return postalAddresses.collectList();
//
//                        });
        Mono<Customer> monoCustomer =
                this.getCustomerById(customerId)
                        .zipWhen(customer -> {

                            Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(customer.getId());
                            return postalAddresses.collectList();

                        })
                        .map(tuple2 -> {

                            Customer c = tuple2.getT1();
                            List<CustomerAddress> a = tuple2.getT2();
                            c.setAddresses(a);

                            return c;
                        });


        return monoCustomer;
//        Flux<Customer> customers = null;


//        return customers;

    }


}
