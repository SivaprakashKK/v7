package com.verizon.learning;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

public class BackPressureDemo {

    @Test
    public void back_presssure_demo() {

        Flux<Integer> ints = Flux.range(0, 15);

        ints
                .log()
                .subscribe(new BaseSubscriber<Integer>() {

                    @Override
                    protected void hookOnSubscribe(Subscription subscription) {

                        subscription.request(6);
//                        super.hookOnSubscribe(subscription);

                    }

                    @Override
                    protected void hookOnNext(Integer value) {

                        if (value > 0 && value % 5 == 0) {

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println(Thread.currentThread().getName());
                            super.request(5);
                        }
                    }
                });


    }
}
