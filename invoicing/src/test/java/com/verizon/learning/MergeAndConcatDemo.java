package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

public class MergeAndConcatDemo {


    @Test
    public void merge_and_concat_demo() throws InterruptedException {


        Flux<String> f1 = Flux
                .just("f1-1", "f1-2", "f1-3")
//                .delayElements(Duration.ofSeconds(1))
                .publishOn(Schedulers.elastic())
                .map(x -> {

                    System.out.println(x + " -- " + " f1 : " + Thread.currentThread().getName());
                    return x ;
                });


        Flux<String> f2 =
                Flux
                        .just("101", "102", "103")
//                        .delayElements(Duration.ofSeconds(1))
                        .publishOn(Schedulers.elastic())
                        .map(x -> {

                            System.out.println(x + " -- " + " f2 : "+ Thread.currentThread().getName());
                            return x ;
                        });


        Flux<String> f3 = Flux.concat(f2, f1);
        Flux<String> f4 = Flux.merge(f2, f1);


        f3.
                subscribe(x -> {
//                    System.out.println(x);
                });


        Thread.sleep(10000);


    }

}
