package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;

public class ZipDemo {


    @Test
    public void zip_demo() throws InterruptedException {


        Flux<Long> f1 = Flux.interval(Duration.ofMillis(1000));

        Flux<String> f2 = Flux.just("f2-1", "f2-2", "f2-3");

        Flux<String> f3 =
                f1
                        .zipWith(f2,  (valuePublishedByF1, valuePublishedByF2) -> {

                            return valuePublishedByF1 + " -- " + valuePublishedByF2;

                        });


        f3
                .log().subscribe();
//                .subscribe(x -> {
//
////                    System.out.println(x.getT1() + " -- " + x.getT2());
//                    System.out.println(x);
//                });


        Thread.sleep(5000);


    }
}
