package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class StepVerifierDemo {

    @Test
    public void step_verifier() {

        Flux<Integer> f1 = Flux.range(0, 5);

        StepVerifier.create(f1)
                .expectSubscription()
                .expectNextCount(5)
                .verifyComplete();


    }
}
