package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

public class HandlingErrors {

    @Test
    public void on_continue() {


//        Flux<Integer> f1 = Flux.just(1, 2, 3, 4, 5, 6)
//                .map(x -> {
//
//                    if (x == 2) {
//                        throw new RuntimeException(x + " could not processed");
//                    }
//                    return x + 1;
//                }).onErrorContinue((x , y) -> {
//
//                    System.out.println(x.getMessage() + " -- " + y);
//
//                });

        Flux<Integer> f1 = Flux.just(1, 2, 3, 4, 5, 6)
                .map(x -> {

                    if (x == 2) {
                        throw new RuntimeException(x + " could not processed");
                    }
                    return x + 1;
                }).onErrorResume(x -> {

                    System.out.println(" ----- " + x.getMessage() + " -- ");

                    return Flux.just(10, 20, 30);

                });


        f1.subscribe(x -> {
            System.out.println(x);
        });


    }
}
