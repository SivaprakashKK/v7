package learningreactive;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

public class SchedulersDemo {

	public static void main(String[] args) throws InterruptedException {

		ExecutorService service = Executors.newCachedThreadPool();

		Scheduler scheduler = Schedulers.parallel();

		int totalTasks = 64;
		
		CountDownLatch latch = new CountDownLatch(totalTasks);
		long start = System.currentTimeMillis();

		for (int i = 0; i < totalTasks; i++) {

			int j = i;
			scheduler.schedule(new Runnable() {

				@Override
				public void run() {

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println(j + " Thread : " + Thread.currentThread().getName());
				 
					latch.countDown();

				}
				

			});
		}

		latch.await();
		long end = System.currentTimeMillis();
		System.out.println("After all tasks executed :  " + (end - start));

	}

}
