package learningreactive;

import java.time.Duration;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxDemo {

	public static void main(String[] args) throws InterruptedException {
				
		// @formatter:off
				
		Mono<String> mString = Mono.just("101");
		
		Flux<Long> fString = 
				Flux
				  .interval(Duration.ofSeconds(1))
				  .map(x -> x + 1 )
				  .filter(x -> x > 1);

		
		fString
		  .subscribe(x -> {
			  System.out.println(x);
		  });
		
		 
		Thread.sleep(10000);
		// @formatter:on

	}

}
