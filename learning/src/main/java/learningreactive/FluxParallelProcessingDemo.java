package learningreactive;

import java.util.Arrays;
import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;

public class FluxParallelProcessingDemo {

	public static void main(String[] args) throws InterruptedException {

		List<String> clients = Arrays.asList("Nomura", "Verizon", "Morgan Stanley", "Morgan", "JP Morgan",
				"BNP Paribas");

		// @formatter:off
		
		ParallelFlux<String> fluxOfString = 
				Flux.fromIterable(clients)
				    .parallel()
				    .runOn(Schedulers.elastic())

				    .map(x -> {
				    	
				    	try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	
				    	System.out.println(x + " thread : " + Thread.currentThread().getName());
				    	return x.toLowerCase();
				    });
				    
				    
		
		fluxOfString
		  .subscribe();
		
		Thread.sleep(10000);
		// @formatter:on

	}

}
