package learning;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RunnableTasksAndFuture {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService service = Executors.newFixedThreadPool(5);

		Future<String> futureObject = service.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {

				// call to service 1

				Thread.sleep(5000);

				return "service-1-response";
			}
		});

		String value = futureObject.get();
		System.out.println("received value : " + value + " -- " + Thread.currentThread().getName());

		service.shutdown();

	}

}
