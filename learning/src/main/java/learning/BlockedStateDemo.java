package learning;

public class BlockedStateDemo {

	public static void main(String[] args) throws InterruptedException {

		Counter counter = new Counter();

		Thread t1 = new Thread() {
			public void run() {
				counter.increment();

			};
		};
		
		t1.start();
		Thread.sleep(200);
		Thread t2 = new Thread() {

			public void run() {
				counter.increment();

			};
		};
		
		t2.start();
		
		
		new Thread() {
			public void run() {
				while (true) {
				
					System.out.println("t2 state :"+ t2.getState());
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			};
		}.start();
		
		

	}

}

class Counter {

	private int c;

	public synchronized void increment() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c++;
	}

}
