package learning;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;

public class CompletableFutureDemo {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println(Runtime.getRuntime().availableProcessors());
		System.out.println(ForkJoinPool.commonPool().getParallelism());

		CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
		
			//service 1 is called
			System.out.println("This task is executing in : " + Thread.currentThread().getName()  + " --" + Thread.currentThread().isDaemon());
			try {
				
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "response-from-service-1";
		});
		

		cf.whenComplete((x , y) -> {
			if (y == null) {
				System.out.println("the value received is : " + x);
				
			}
		}).handle((x, y) -> {
			
			return "some value" + " -- " + x + " -- " + y;
		});
		
		System.out.println("Yabadabaadoo....");
		
		
		Thread.sleep(10000);

	}

}
