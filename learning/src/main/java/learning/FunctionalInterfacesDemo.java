package learning;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionalInterfacesDemo {

    public static void main(String[] args) {

//		TaxCalculator t = (x, y) -> 5;

        Stream<String> s = Stream.of("nomura", "bnp paribas", "verizon");

        Function<String, String> f1 = x -> x.toUpperCase();
        Predicate<String> f2 = x -> x.contains("VERI");
        Supplier<String> f3 = () -> "Verizon";
        
        Consumer<String> f4 = x -> {System.out.println(x);};
        
        BiConsumer<String, String> f5 = (x, y) -> {};
        
        BiFunction<String, String, String> f6 = (x, y) -> x + " -- " + y;
         

        // @formatter:off

        List<String> lens =
                s
                        .map(x -> x.toUpperCase())
                        .filter(x -> x.contains("VERI"))
                        .collect(Collectors.toList());


        // @formatter:on

        System.out.println(lens);

//		generateTaxSlips(t);

    }

    /**
     * Higher Order functions
     *
     * @param calculator
     */
    public static void generateTaxSlips(TaxCalculator calculator) {

//		double v = calculator.calcualteTax("MHA");
        /**
         *
         */

    }

}

@FunctionalInterface
interface TaxCalculator {

    //	public double calcualteTax(String state);
    public double calcualteTax(String state, Integer rate);

    public default double getDefaulTax() {
        return 18;
    }

}
