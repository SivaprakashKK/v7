package learning;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsDemo {

	public static void main(String[] args) {

		List<String> clients = Arrays.asList("Nomura", "Verizon", "Morgan Stanley", "Morgan", "JP Morgan",
				"BNP Paribas");
		
		long start = System.currentTimeMillis();
		
		// @formatter:off
		List<String> transformedClients =  clients
		   .stream()
		   .parallel()
		   .map(x -> {
			   
			   try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			   System.out.println(x + " -- " +Thread.currentThread().getName());
			   
			   return x.toLowerCase();
		   })
		   .filter(x -> x.contains("morgan"))
		   .collect(Collectors.toList());
		// @formatter:on

		
		long end = System.currentTimeMillis();
		
		System.out.println(transformedClients + " -- " + (end - start));
		

	}

}
