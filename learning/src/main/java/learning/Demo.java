package learning;

public class Demo {

	public static void main(String[] args) throws ClassNotFoundException {

		new Thread() {
			
			
			{
//				super.setDaemon(true);
			}
			
			@Override
			public void run() {
				System.out.println("Started Thread : " + Thread.currentThread().getName() + " -- " + Thread.currentThread().isDaemon());
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				System.out.println("Exiting Thread : " + Thread.currentThread().getName());
			}
		}.start();
		
		
		System.out.println("This is : " + Thread.currentThread().getName() + " -- " + Thread.currentThread().isDaemon());
	}

}
