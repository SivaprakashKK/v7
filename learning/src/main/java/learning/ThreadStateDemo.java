package learning;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ThreadStateDemo {

	// @formatter:off

	/**
	 *  NEW !
	 * 
	 *  Runnable !
	 * 
	 *  Blocked !
	 *  
	 *  Waiting !
	 *  
	 *  Timed_Waiting !
	 *  
	 *  Terminated ! 	 * 
	 * 
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		 
		// @formatter:on
		
		ServerSocket ss = new ServerSocket(7001);
		
		Thread t1 = new Thread() {
			
			public void run() {
			
				try {
					
					System.out.println(Thread.currentThread().getName() +  " Listening for incoming request");
					Socket s = ss.accept();
					System.out.println("received request....");
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
		
		t1.start();
		
		
		new Thread() {
			public void run() {
				while (true) {
				
					System.out.println("t1 state :"+ t1.getState());
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			};
		}.start();
		
		
		
		
		
		

	}

}










