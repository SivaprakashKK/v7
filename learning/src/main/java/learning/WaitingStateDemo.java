package learning;

import java.util.ArrayList;
import java.util.List;

public class WaitingStateDemo {
	
	public static void main(String[] args) throws InterruptedException {
		
		MyBoundedDataStructure ds = new MyBoundedDataStructure(3);
		ds.put("v1");
		ds.put("v2");
		ds.put("v3");
		
		
		
		Thread t1 = new Thread() {
			public void run() {
				ds.put("v4");

			};
		};		
		t1.start();
		
		Thread.sleep(200);
		Thread t2 = new Thread() {

			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(ds.take());
			};
		};
		
		t2.start();
		
		
		new Thread() {
			public void run() {
				while (true) {
				
					System.out.println("t1 state :"+ t1.getState());
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			};
		}.start();
		
		
		
	}

}

class MyBoundedDataStructure {

	private List<String> values = new ArrayList<>();

	private int size;

	public MyBoundedDataStructure(int size) {

		this.size = size;

	}
	
	public synchronized void put(String value) {
		if (values.size() == size) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.values.add(value);
	}
	
	public synchronized String take() {
		String value = this.values.remove(0);
		notify();
		return value;
	}

}
