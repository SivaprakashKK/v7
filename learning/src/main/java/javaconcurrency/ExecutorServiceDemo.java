package javaconcurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceDemo {

	public static void main(String[] args) throws InterruptedException {

		ExecutorService service = Executors.newCachedThreadPool();
		int totalTasks = 10;

		CountDownLatch latch = new CountDownLatch(totalTasks);
		long start = System.currentTimeMillis();

		for (int i = 0; i < totalTasks; i++) {
			int j = i;
			service.execute(new Runnable() {

				@Override
				public void run() {

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println(j + " thread : " + Thread.currentThread().getName());

					latch.countDown();
				}

			});
		}

		latch.await();
		long end = System.currentTimeMillis();
		System.out.println("total time taken is : " + (end - start));

		service.shutdown();

	}

}
