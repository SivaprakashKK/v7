package javaconcurrency;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorDemo {

	public static void main(String[] args) throws InterruptedException {

//	ThreadPoolExecutor tpe = new ThreadPoolExecutor(5, 5, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<>(100), new ThreadFactory() {
//			
//			@Override
//			public Thread newThread(Runnable r) {
//				
//				return null;
//			}
//		}, new RejectedExecutionHandler()  {
//
//			@Override
//			public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
//				
//				
//			}
//			
//		});

		ThreadPoolExecutor tpe = new ThreadPoolExecutor(10, 10, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<>(100));
		int totalTasks = 10;

		CountDownLatch latch = new CountDownLatch(totalTasks);
		long start = System.currentTimeMillis();

		for (int i = 0; i < totalTasks; i++) {
			int j = i;
			tpe.execute(new Runnable() {

				@Override
				public void run() {

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println(j + " thread : " + Thread.currentThread().getName());

					latch.countDown();
				}

			});
		}

		latch.await();
		long end = System.currentTimeMillis();
		System.out.println("total time taken is : " + (end - start));

		tpe.shutdown();

	}

}
