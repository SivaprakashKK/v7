package com.verizon.learning.controllers;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;

@RestController
public class DemoController {

	@GetMapping (path ="/demo", produces = {MediaType.APPLICATION_STREAM_JSON_VALUE})
	public Flux<Long> handleDemo() throws InterruptedException, ExecutionException {
		System.out.println("The thread that receives the request  is :" + Thread.currentThread().getName());

		// @formatter:off

		Flux<Long> returnValue = 
				Flux.interval(Duration.ofSeconds(1))
					.map(x -> x + 1)
				    .take(10)
				    .log();
		
//		
//		ExecutorService service = Executors.newCachedThreadPool();
//		Future<String> f = service.submit(new Callable<String>() {
//
//			@Override
//			public String call() throws Exception {
//				Thread.sleep(5000);
//				return "ss";
//			}
//		});
//		
//		String value = f.get();
//		
//		System.out.println("What ever the value :" + value);
		
		
		System.out.println("The thread that recevied the request is leaving : " + Thread.currentThread().getName());

		return returnValue;
		
		
	}
	
	
	

}
