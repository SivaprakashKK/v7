package com.verizon.learning.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.xml.ws.Response;
import java.time.Duration;

@RestController
public class CustomerRestController {

	@Autowired
	CustomerRepository customerRepo;

	@GetMapping(path = "/customers", produces = { MediaType.APPLICATION_STREAM_JSON_VALUE })
	public Flux<Customer> handleGetCustomers() {

//        Flux<Customer> customers =
//                customerRepo
//                        .findAll()
//                        .zipWith(Flux.interval(Duration.ofSeconds(2)), (x, y) -> {
//                            return x;
//                        });

		Flux<Customer> customers = customerRepo.findAll();

		return customers;

	}

	@GetMapping(path = "/customers/{customerId}")
	public Mono<ResponseEntity<Customer>> handleGetCustomer(@PathVariable(name = "customerId") String customerId) {

		Mono<Customer> customer = customerRepo.findById(customerId);

		Mono<ResponseEntity<Customer>> response = customer.map(x -> new ResponseEntity<>(x, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NO_CONTENT));

		return response;

	}

}
