package com.verizon.learning.controllers;

import com.verizon.learning.model.CustomerAddress;
import com.verizon.learning.model.CustomerAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class CustomerAddressRestController {

    @Autowired
    private CustomerAddressRepository addressRepo;

//    @GetMapping(path = "/customer-addresses")
//    public Flux<CustomerAddress> handleGetCustomerAddresses() {
//
//        return
//                addressRepo.findAll();
//
//    }

    @GetMapping(path = "/customers/{customerId}/addresses", params = {"type"})
    public Flux<CustomerAddress>
    handleGetAddressesByCustomerIdAndType(@PathVariable String customerId,
                                          @RequestParam String type) {

        Flux<CustomerAddress> addresses = addressRepo.findByCustomerIdAndType(customerId, type);

        return addresses;
    }


}


