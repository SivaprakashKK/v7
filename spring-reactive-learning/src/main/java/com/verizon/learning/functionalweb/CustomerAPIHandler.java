package com.verizon.learning.functionalweb;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class CustomerAPIHandler {

    @Autowired
    CustomerRepository customerRepo;

    private static Mono<ServerResponse> notFound = ServerResponse.notFound().build();

    public Mono<ServerResponse> getAllCustomers(ServerRequest serverRequest) {

        // @formatter:off

        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(customerRepo.findAll(), Customer.class);


        // @formatter:on

    }

    public Mono<ServerResponse> getCustomer(ServerRequest serverRequest) {

        // @formatter:off

        String customerId = serverRequest.pathVariable("id");

        Mono<Customer> customer = customerRepo.findById(customerId);

                return customer.flatMap(x -> {

                    return ServerResponse.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(BodyInserters.fromPublisher(Mono.just(x), Customer.class));
                })
                .switchIfEmpty(notFound);

        // @formatter:on

    }

    public Mono<ServerResponse> createCustomer(ServerRequest serverRequest) {

        Mono<Customer> customer = serverRequest.bodyToMono(Customer.class);
        Mono<Customer> newCustomer = customer.flatMap(x -> customerRepo.save(x));

        // @formatter:off

        Mono<ServerResponse> response =

                ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(newCustomer, Customer.class);

        /**
         *
         * Below is more elegant way.The idea is to identify what is it that you want to
         * return back ./ More often than not, it is Mono<ServerResponse> which can be
         * built using ServerResponse.OK() and specifiying what to put in body
         *
         */

//		Mono<ServerResponse> response = customer.flatMap(x -> {
//			
//						return ServerResponse.ok()
//								.contentType(MediaType.APPLICATION_JSON)
//								.body(customerRepo.save(x), Customer.class);
//			
//					});
//		


        // @formatter:on

        return response;
    }

    public Mono<ServerResponse> deleteCustomer(ServerRequest serverRequest) {

        String customerId = serverRequest.pathVariable("id");

        Mono<Void> deletedCustomer = customerRepo.deleteById(customerId);

        // @formatter:off

        Mono<ServerResponse> response =

                ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(deletedCustomer, Void.class);

        return response;
    }


//    public Mono<ServerResponse> updateCustomer(ServerRequest serverRequest) {
//
//        // @formatter:off
//
//        String customerId = serverRequest.pathVariable("id");
//        Mono<Customer> updateExistingCustomerWithThis = serverRequest.bodyToMono(Customer.class);
//        Mono<Customer> updatedCustomer = updateExistingCustomerWithThis.flatMap(x -> {
//            Mono<Customer> existingCustomer = customerRepo.findById(customerId);
//            Mono<Customer> customerWithUpdatedValues = existingCustomer.flatMap(y -> {
//
//                y.setName(x.getName());
//                y.setAddress(x.getAddress());
//                return customerRepo.save(y);
//            });
//            return customerWithUpdatedValues;
//        });
//        Mono<ServerResponse> response =
//                updatedCustomer.flatMap(x -> {
//                    return ServerResponse.ok()
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .body(BodyInserters.fromPublisher(Mono.just(x), Customer.class));
//                }).switchIfEmpty(notFound);
//
//        // @formatter:on
//        return response;
//    }

}
