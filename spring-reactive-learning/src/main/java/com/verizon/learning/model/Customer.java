package com.verizon.learning.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Customer {

	@Id
	private String id;

	private String name;

	private List<CustomerAddress> addresses;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(String id, String name) {

		this.id = id;
		this.name = name;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<CustomerAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CustomerAddress> addresses) {
		this.addresses = addresses;
	}
	
	
	@Override
	public String toString() {
		
		return this.id + " -- " + this.name;
	}


}
