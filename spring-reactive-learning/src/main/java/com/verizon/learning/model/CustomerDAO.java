package com.verizon.learning.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class CustomerDAO {


    @Autowired
    CustomerRepository customerRepo;

    public Flux<Customer> getCustomers() {

        Flux<Customer> customers = customerRepo.findAll();

        return customers;

    }

    public void getCustomerById(String customerId) {

        Mono<Customer> customerMono = customerRepo.findById(customerId);

//		if (optCustomer.isPresent()) {
//			// the object was found
//		} else {
//			// the object was NOT found
//		}

    }

}
