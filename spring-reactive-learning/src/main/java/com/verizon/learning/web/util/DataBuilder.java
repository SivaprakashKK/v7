package com.verizon.learning.web.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.verizon.learning.model.CustomerAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerAddress;
import com.verizon.learning.model.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DataBuilder implements CommandLineRunner {

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    CustomerAddressRepository addressRepo;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("CommandLine runner is executed...");
        initialize();
    }

    /**
     * Create a Customer model class Create CustomerRepository We have used
     * customerRepo to delete + insert + findAll
     * <p>
     * CustomerAddress model class is already there Create CustomerAddressRepository
     * Wire this CustomerAddressRepository to this DataBuilder in the intialize
     * function please use addressRepo to add the address to the database exactly
     * the way customers are added
     */
    private void initialize() {

        /**
         * 1st delete all the records from Customer table
         * 2nd : I want to insert all the customers object that are there in the list
         * 3rd : I want to findAll and display all the newly inserted data.
         *
         */

        // @formatter:off


//		Flux<Mono<Customer>> customers = 
//				  Flux.fromIterable(this.customerData())
//				  .map(x -> {
//					  
//					  Mono<Customer> monoOfCustomer =  customerRepo.save(x);
//					  
//					  return monoOfCustomer;
////					  return x;
//					  
//				  });
//
//
//		Mono<Void> deleteAll = customerRepo.deleteAll();
//		
//		
//		Flux<Customer> customers = 
//				  Flux.fromIterable(this.customerData())
//				  .flatMap(x -> {
//					  
//					  Mono<Customer> monoOfCustomer =  customerRepo.save(x);
//					  
//					  return monoOfCustomer;
//					  
//				  });
//		
//		
//		Flux<Customer> newCustomers = customerRepo.findAll();
//		
//		newCustomers.subscribe(x -> {
//			System.out.println(x);
//		});
//		List<Customer> newlyAddedCustomers = new ArrayList<>();

        System.out.println("1 > Thread : " + Thread.currentThread().getName());
        customerRepo
                .deleteAll()
                .thenMany(Flux.fromIterable(this.customerData()))
                .flatMap(x -> customerRepo.save(x))
                .thenMany(customerRepo.findAll())
                .subscribe(x -> {
                    System.out.println(x + " " + Thread.currentThread().getName());

                });


        addressRepo
                .deleteAll()
                .thenMany(Flux.fromIterable(this.addressData()))
                .flatMap(x -> addressRepo.save(x))
                .thenMany(addressRepo.findAll())
                .subscribe(x -> {
                    System.out.println(x);
                });


        System.out.println("2 > Thread : " + Thread.currentThread().getName());


        // @formatter:on


    }

    public List<CustomerAddress> addressData() {

        // @formatter:off


        return Arrays.asList(new CustomerAddress("9", "101", "101 line1", "101 line2", "101 city", "P"),
                new CustomerAddress("1", "101", "101 line1-a1", "101 line2-a1", "101 city-a1", "P"),
                new CustomerAddress("2", "105", "105 line1-a1", "105 line2", "105 city-a2", "D"),
                new CustomerAddress("3", "102", "102 line1", "102 line2", "102 city", "P"),
                new CustomerAddress("4", "103", "103 line1-1", "103 line2-1", "103 city-1", "P"),
                new CustomerAddress("5", "103", "103 line1-2", "103 line2-2", "103 city-2", "D"),
                new CustomerAddress("6", "103", "103 line1-3", "103 line2-3", "103 city-3", "P"),
                new CustomerAddress("7", "104", "104 line1", "104 line2", "104 city", "D"));

    }

    public List<Customer> customerData() {
        // @formatter:off

        return Arrays.asList(new Customer("101", "Verizon"),
                new Customer("102", "IBM"),
                new Customer("103", "Wipro"),
                new Customer("104", "Morgan"),
                new Customer("105", "Morgan Stanley"));


        // @formatter:on

    }

    public static void main(String[] args) {

        DataBuilder d = new DataBuilder();
        List<Customer> customers = d.customerData();

        /**
         * 1) transform : 1 object maps to exactly 1 object [of any datatype] : map
         * 2) transform : 1 (customer) objects maps to more than 1 objects (CustomerAddress) :flatMap
         *
         */

        // @formatter:off

//		List<List<CustomerAddress>> customerNames = 
//				customers
//				.stream()
//				.map(x -> x.getAddresses())
//				.collect(Collectors.toList());

        List<CustomerAddress> customerNames =
                customers
                        .stream()
                        .flatMap(x -> x.getAddresses().stream())
                        .collect(Collectors.toList());


        System.out.println(customerNames);

    }


}
